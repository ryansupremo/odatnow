{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection
{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Developer Queue</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Brand Name</th>
                    <th>SKU</th>
                    <th>Bug</th>
                    <th>New Feature</th>
                    <th>Question</th>
                    <th>Other</th>
                    <th>Comments</th>
                    <th>User</th>
                    <th>Date Opened</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                @if ($data->count())
                    @foreach ($data as $index => $i)
                        <tr>
                            <td>{{ $i->brand_name }}</td>
                            <td>{{ $i->sku }}</td>
                            <td>
                                @if($i->bug == "on")
                                    <i class="text-success fa fa-check fa-2x" aria-hidden="true"></i>
                                @else
                                    <i class="text-danger fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($i->new_feature == "on")
                                    <i class="text-success fa fa-check fa-2x" aria-hidden="true"></i>
                                @else
                                    <i class="text-danger fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($i->question == "on")
                                    <i class="text-success fa fa-check fa-2x" aria-hidden="true"></i>
                                @else
                                    <i class="text-danger fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($i->other == "on")
                                    <i class="text-success fa fa-check fa-2x" aria-hidden="true"></i>
                                @else
                                    <i class="text-danger fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($i->comments != "")
                                    <p>{{$i->comments}}</p>
                                @endif
                            </td>
                            <td>{{ $i->name }}</td>
                            <td>{{ $i->date_added }}</td>
                            <td>{{ $i->status }}</td>
                            <td class="nowrap">
                                <a href="{{ route('admin.developerqueue.edit', $i->id) }}" class="btn btn-primary">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
            {{ $data->appends(request()->only('_id'))->links() }}
        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
