@extends('layout.default')

@section('styles')
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Admin : Manage Brands : New</h3>
                </div>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <form action="{{ route('admin.brands.save') }}" method="POST">
                            @csrf
                            <div class="row mt-2">
                                <div class="col-8">
                                    <label for="brand_id">Brand ID</label>
                                    <input type="text" name="brand_id" id="brand_id" class="form-control" placeholder="Brand name" value="{{@old('brand_id')}}" required>
                                    @error('brand_id')
                                        <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-8">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Brand name" value="{{@old('name')}}" required>
                                    @error('name')
                                        <small class="text-danger font-weight-bold">* {{ $message }}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-8">
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-8">
                                    <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                                    <a href="{{ route('admin.flags.list') }}" class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
