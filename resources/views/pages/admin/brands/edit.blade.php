@extends('layout.default')

@section('styles')
@endsection

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Admin : Manage Brands : Edit</h3>
                </div>
            </div>

            <div class="card-body">
                @if ($message = session('message'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        {{ $message }}
                    </div>
                @endif
                <div class="form-group">
                    <p class="font-weight-bold text-uppercase">Brand ID: <span class="text-primary">{{$brand->brand_id}}</span></p>
                </div>
                <form action="{{ route('admin.brands.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$brand->id}}">

                    <div class="row mt-2">
                        <div class="col-8">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" value="{{$brand->name}}" class="form-control" required="required">
                            @error('name')
                                <small class="text-danger font-weight-bold">* {{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-8">
                            <input type="submit" value="Update" class="btn btn-primary">&nbsp;
                            <a href="{{ route('admin.brands.list') }}" class="btn btn-warning">Cancel</a>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
