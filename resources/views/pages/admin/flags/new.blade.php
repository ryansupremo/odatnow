{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
@endsection

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Manage Flags  : New</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('admin.flags.save') }}" method="POST">
                @csrf
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required="required">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <label for="platform">Platform</label>
                        <select name="platform" id="platform" class="form-control" required="required">
                            <option value="">Select</option>
                            <option value="Amazon">Amazon</option>
                            <option value="Ebay">Ebay</option>
                            <option value="Shopping">Shopping</option>
                            <option value="General">General</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                        <a href="{{ route('admin.flags.list') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
