{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Users : New</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('admin.users.save') }}" method="POST">
                @csrf
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="password1">Password</label>
                        <input type="password" name="password1" id="password1" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="password2">Verify Password</label>
                        <input type="password" name="password2" id="password2" class="form-control" required="required">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="roles">Security Group</label>
                        <select name="roles[]" class="form-control" id="roles" multiple size="5" required="required">
                            <option value="">Select one or more</option>
                            <optgroup label="----------------------------------------------"></optgroup>
                            @if ($roles->count())
                                @foreach ($roles as $index => $i)
                                    <option value="{{ $i->slug }}">{{ $i->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                        <a href="{{ route('admin.users.list') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
