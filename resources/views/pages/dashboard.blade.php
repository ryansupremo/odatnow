{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Dashboard</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">

                        @if ($message = session('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                {{ $message }}
                            </div>
                        @endif

                        @if ($error = session('error'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                {{ $error }}
                            </div>
                        @endif

                        @if ($warning = session('warning'))
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                {{ $warning }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    {{--<script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>--}}
@endsection
