{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <style>
        .nopadding {
            padding: 0 !important;
        }

        .content {
            padding: 20px;
        }

        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: Notifications</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $message }}
                </div>
            @endif

            @if ($error = session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $error }}
                </div>
            @endif

            @if ($warning = session('warning'))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $warning }}
                </div>
            @endif

            <h3>Add Notification</h3>
            <form action="{{ route('products.notifications.save') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="row mt-2">
                <div class="col">
                    <label for="reason">Reason</label>
                    <textarea name="reason" id="reason" class="form-control"></textarea>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <label for="notes">Notes</label>
                    <textarea name="notes" id="notes" class="form-control"></textarea>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <label>Dismiss</label>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <label class="switch">
                        <input type="checkbox" name="dismiss" value="on">
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <label>Snooze</label>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <label class="switch">
                        <input type="checkbox" name="snooze" value="on">
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <input type="submit" value="Add Notification" class="btn btn-success btn-lg">&nbsp;
                    <a href="{{ route('products.optimization', $product->id) }}" class="btn btn-warning btn-lg">Cancel</a>
                </div>
            </div>

            </form>


        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
