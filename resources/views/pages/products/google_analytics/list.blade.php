@extends('layout.default')


@section('styles')
    <link rel="stylesheet" href="/css/customCss/pages.css">
    <link rel="stylesheet" href="/css/customCss/style.css">
@endsection

@section('content')
        <div class="row mb-5">
            <div class="col">
                <div class="card card-custom border-radius-10 py-0 px-5 ">
                    <div class="card-header border-0 pl-1 py-5" style="min-height: auto">
                        <div class="card-title m-0">
                            <h3 class="card-label">Products :: Analytics</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card p-4 border-radius-10">
                    <div class="row">
                        <div class="col">
                            <h4>Yesterday</h4>
                            users: {{$users}} <br>
                            page views: {{($pageviews ?? 0)}} <br>
                            sessions: {{$sessions ?? 0}} <br>
                        </div>
                        <div class="col">
                            <h4>Last 7 days</h4>
                            users: {{$users7 ?? 0}} <br>
                            page views: {{$pageviews7}} <br>
                            sessions: {{$sessions7}} <br>
                        </div>
                        <div class="col">
                            <h4>Last 30 days</h4>
                            users: {{$users30}} <br>
                            page view: {{$pageviews30}} <br>
                            sessions: {{$sessions30}} <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection



@section('scripts')
    <script src="/js/customJs/google_analytics/index.js"></script>
@endsection
