<i
    class="fas fa-check-square fa-2x "
    @if($product->status == "complete")
    style="color:#377032"
    @else
    style="color:#8a1937"
    @endif
    @if($product->status == "complete")
    onclick="ajax_get_update(this.form, '{{ route('products.ajax.complete', [$product->id, 'review']) }}', '#optimization_complete');"
    @else
    onclick="ajax_get_update(this.form, '{{ route('products.ajax.complete', [$product->id, 'complete']) }}', '#optimization_complete');"
    @endif
></i>
