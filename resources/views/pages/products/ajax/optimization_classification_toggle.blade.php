<div class="btn-group btn-group-toggle" data-toggle="buttons">
    <label class="btn
    @if(isset($product->classification))
        @if($product->classification == "rare")
            btn-success
        @else
            btn-secondary
        @endif
    @else
        btn-secondary
    @endif
    ">
    <input type="radio"
        name="classification"
        id="classification_rare"
        value="rare"
        onclick="ajax_update(this.form, '{{ route('products.ajax.classification') }}', '#ajax_classification');"
        @if(isset($product->classification))
            @if($product->classification == "rare")
                checked
            @endif
        @endif
        autocomplete="off"> Rare
        </label>
        <label class="btn
        @if(isset($product->classification))
            @if($product->classification == "common")
                btn-success
            @else
                btn-secondary
            @endif
        @else
            btn-secondary
        @endif
        ">
        <input type="radio"
            name="classification"
            id="classification_common"
            value="common"
            onclick="ajax_update(this.form, '{{ route('products.ajax.classification') }}', '#ajax_classification');"
               @if(isset($product->classification))
                @if($product->classification == "common")
                    checked
                @endif
            @endif
            autocomplete="off"> Common
    </label>
</div>
