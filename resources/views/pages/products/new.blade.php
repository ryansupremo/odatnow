{{-- Extends layout --}}
@extends('layout.default')

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="/css/customCss/style.css">
<link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
<!-- new blade -->

<div id="product" class="row mb-2 pl-5 pr-4 justify-content-center">
    <div class="card card-custom col-12 col-md-12 col-lg-12 title bg-white w-100 py-3 px-5 d-flex justify-content-between">
        <div class="card-header pl-1 pt-0 flex-wrap border-0 pb-0 d-flex justify-content-between" style="min-height: auto;">
            <div class="card-title">
                <h3 class="card-label mb-0"><a href="{{ route('products.list') }}">Products</a> :: Product Add Wizard - Step 1 of 10</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex flex-column">
        @if ($message = session('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $message }}
        </div>
        @endif

        @if ($error = session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $error }}
        </div>
        @endif

        @if ($warning = session('warning'))
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $warning }}
        </div>
        @endif
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}
@endsection

