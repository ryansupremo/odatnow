{{-- Extends layout --}}
@extends('layout.default')

{{-- CSS --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card card-custom ">
            <div class="card-header flex-wrap border-0 pt-6 pb-0 d-flex justify-content-between">
                <div class="card-title">
                    <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: Alarm</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                        <h1 class="font-weight-bold">Alarm</h1>
                    </div>
                </div>
                @if ($message = session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $message }}
                </div>
                @endif

                @if ($error = session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $error }}
                </div>
                @endif

                @if ($warning = session('warning'))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $warning }}
                </div>
                @endif

                <form class="p-3" action="{{ route('products.alarm.save') }}" method="POST">
                    <input type="hidden" name="id" value="{{ $id }}">
                    @csrf
                    <div class="row flex-column flex-md-row flex-lg-row mt-2">
                        <div class="col-12 col-md-6 col-lg-6 mb-5">
                            {{-- Left Side --}}
                            <div class="row">
                                <div class="col">
                                    <label for="alarm_date">Alarm Date</label>
                                    <input type="date" name="alarm_date" id="alarm_date" @if(isset($data)) @if(count($data)> 0)
                                    value="{{ $data[0]->alarm_date }}"
                                    @endif
                                    @endif
                                    class="form-control"
                                    >
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <label for="comments">Comments</label>
                                    <textarea name="comments" rows="5" id="comments" class="form-control">
                                        @if(isset($data))
                                            @if(count($data) > 0)
                                                {{$data[0]['comments']}}
                                            @endif
                                        @endif
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6 mb-5">
                            {{-- Right Side --}}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="market_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->market_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="market_check">
                                        <label for="market_check">Market Check</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="price_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->price_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="price_check">
                                        <label for="price_check">Price Check</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="seo_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->seo_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="seo_check">
                                        <label for="seo_check">SEO Check</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="orders_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->orders_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="orders_check">
                                        <label for="orders_check">Orders Check</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="velocity_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->velocity_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="velocity_check">
                                        <label for="velocity_check">Velocity Check</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="buy_box_review" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->buy_box_review == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="buy_box_review">
                                        <label for="buy_box_review">Buy Box Review</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="competitor_check" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->competitor_check == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="competitor_check">
                                        <label for="competitor_check">Competitor Check</label>
                                    </div>
                                    <div class="form-group alarmCheckboxes">
                                        <input type="checkbox" name="other" @if(isset($data)) @if(count($data)> 0)
                                        @if($data[0]->other == "on")
                                        checked
                                        @endif
                                        @endif
                                        @endif
                                        id="other">
                                        <label for="other">Other</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <input type="submit" value="Save" class="btn btn-primary">&nbsp;
                            <a href="{{ route('products.optimization', $id) }}" class="btn btn-warning">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
{{-- vendors --}}


@endsection
