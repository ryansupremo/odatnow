// delete image
Dropzone.autoDiscover = false;
$(document).on('click', '.delete', function () {
    imageID = $(this).attr('data-id')
    product_id = $(this).attr('data-product')
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
        $.ajax({
            url: `/products/image/draft/deleteimage/${imageID}`,
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                "Content-Type": "application/x-www-form-urlencoded"
            },
            type: "GET",
            dataType: 'JSON',
        })
        .then(res => {
            if (res.success === 200) {
                getImagesForProduct(product_id)
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
    }

  })

})

// change from main to secondary and oposite
$(document).on('click', '.changeType', function(){
    let type = $(this).attr('data-type')
    let imageID = $(this).attr('data-id')
    let product_id = $(this).attr('data-product')
    Swal.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#107671',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change it!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/products/image/draft/changeimagetype/${imageID}/${type}`,
                headers: {
                    'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                },
                type: "GET",
                dataType: 'JSON',
            })
            .then(res => {
                if (res.success === 200) {
                    getImagesForProduct(product_id)
                    Swal.fire(
                        'Updated!',
                        `${res.message}`,
                        'success'
                    )
                }
            })
        }
    })
})
$(document).on('mouseover', '.customToolTipIcon', function (e) {
    let id = $(this).attr('data-id')
    $(`#tooltip${id}`).show()
    $(document).on('mouseleave', '.customToolTipIcon', function () {
        $(`#tooltip${id}`).hide()
    })
})

$('.takeSnap').hide()
$('.submitForm').hide()

// camera snap bellow
$('.openCamera').click(function (e) {
  product_id = $(this).attr('data-product')
  $(`#takeSnap${product_id}`).show()
  $(`#submitForm${product_id}`).show()

        Webcam.set({
            width: 320,
            height: 240,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach(`#my_camera${product_id}`);
})

function take_snapshot() {
  Webcam.snap(function (data_uri) {
    $(`#file${product_id}`).val(data_uri);
    document.getElementById(`preview${product_id}`).innerHTML = '<img width="320" height="240" src="' + data_uri + '"/>';
    let data = {
        file: $(`#file${product_id}`).val(),
        type: "secondary",
        id:product_id,
        base64: true,
    }
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            "Content-Type": "application/x-www-form-urlencoded"
        },
        type: "POST",
        dataType: 'JSON',
        url: "/products/image/upload",
        data: data
    })
  });

}

// fetch all images
function getImagesForProduct(id)
{
    $.get(`/products/image/draft/${id}`)
    .then(res => {
        if (res.success === 200) {
            function compare(a, b) {
                const A = a.type.toLowerCase();
                const B = b.type.toLowerCase();
                let comparison = 0;
                if (A > B) {
                    comparison = 1;
                } else if (A < B) {
                    comparison = -1;
                }
                return comparison;
            }

            let test = res.data.sort(compare);
            $(`#productImages${id}`).html('')
            let images = ''
            $.each(test, function (key, value) {
                    let image = `
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="card customCardCol position-relative border" id="productImages${id}" style="width: 18rem;">
                                <img class="card-img-top customCardImage" src="/products/${value.image}">
                                <div class="card-body customCardBody py-3">
                                    <div class="row justify-content-between mb-3">
                                        <div class="col p-1">
                                            <button class="edit btn btn-block btn-warning btn-xs" data-id="${value.id}">
                                                Edit
                                            </button>
                                        </div>
                                        <div class="col p-1">
                                            <button class="delete btn btn-block btn-danger btn-xs" data-product="${id}" data-id="${value.id}">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col p-1 text-left position-relative d-flex align-items-center">
                                            ${ value.type === 'main'
                                                    ?
                                                `<div id="tooltip${value.id}" class="customToolTip text-dark"><i class="fas fa-caret-left mr-3"></i><span class="ml-2">This is main image</span></div>
                                                <i class="customToolTipIcon text-success fas fa-image" data-type="main" data-product="${id}" data-id="${value.id}"></i>`
                                                    :
                                                `<div id="tooltip${value.id}" class="customToolTip text-dark"><i class="fas fa-caret-left mr-3"></i><span class="ml-2">Make main image</span></div>
                                                 <i class="customToolTipIcon fas fa-exchange-alt cursor-pointer changeType" data-type="main" data-product="${id}" data-id="${value.id}" ></i>
                                                `
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `
                    $(`#productImages${id}`).append(image)
            });
        }
    })
}

// dropzone start
$('.openModal').click(function(){
    let openClick = $(this).attr('data-open-count');
    let product_id = $(this).attr('data-id')
    getImagesForProduct(product_id)
    $(`#productModal${product_id}`).modal({
        show: true
    });
    Dropzone.autoDiscover = false
    if (openClick == 0) {
        $(`#myId${product_id}`).dropzone({
            maxFiles: 3,
            thumbnailWidth: "200",
            thumbnailHeight: "200",
            capture: true,
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
            },
            addRemoveLinks: true,
            sending: function (file, xhr, formData) {
                formData.append('type', 'secondary'),
                    formData.append('id', product_id)
            },
            init: function () {
                this.on("success", function (res) {
                    // console.log('response', res.xhr.response)
                    // let image = JSON.parse(res.xhr.response).id
                    getImagesForProduct(product_id)
                })
                this.on("error", function () {
                    // console.log('error', res)
                })
            },
        });
        $(this).attr('data-open-count', 1)
    }
})

//


