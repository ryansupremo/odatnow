$('#picker').daterangepicker({
    ranges: {
        '1 Month': [moment().subtract(1, 'month'), moment()],
        '3 Months': [moment().subtract(3, 'month'), moment()],
        '6 Months': [moment().subtract(6, 'month'), moment()],
        '1 Year': [moment().subtract(12, 'month'), moment()],
        'All': [moment().startOf('year'), moment()],
    },
    "alwaysShowCalendars": true,
    "showCustomRangeLabel": true,
    "autoApply": false,
    // "showDropdowns": true,
    // "startDate": moment(),
    // "endDate": moment(),
    "opens": "left",
}, function(start, end, label) {
    console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    $('#picker').text(start.format('YY/MM/DD') + ' to ' + end.format('YY/MM/DD')).css({'font-size': '0.8rem'})

    // jquery call with values to send to backend will be placed here
});
$('#picker').val(`Date Range `)
