$('#plainSubmit').click(function(e){
    e.preventDefault()
    let id = $('#id').val()
    let images = $('#images').is(':checked') ? "on" : 'off'
    let videos = $('#videos').is(':checked') ? "on" : 'off'
    let blog = $('#blog').is(':checked') ? "on" : 'off'
    let manuals = $('#manuals').is(':checked') ? "on" : 'off'
    let seo = $('#seo').is(':checked') ? "on" : 'off'
    let data_check = $('#data_check').is(':checked') ? "on" : 'off'
    let price_check = $('#price_check').is(':checked') ? "on" : 'off'
    let facebook = $('#facebook').is(':checked') ? "on" : 'off'
    let twitter = $('#twitter').is(':checked') ? "on" : 'off'
    let inventory = $('#inventory').is(':checked') ? "on" : 'off'
    let comments = $('#comments').val()

    let data = {
        id,
        images,
        videos,
        blog,
        manuals,
        seo,
        data_check,
        price_check,
        facebook,
        twitter,
        inventory,
        comments
    }


    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
        },
        type: "POST",
        dataType: 'JSON',
        url: `/products/optimization/assignto/save`,
        data: data,
    })
    .then(res => {
        if (res.success === 200) {
            $('#plainModal').modal('hide')
            $('#comments').val('')
            $('.alert.alert-success').text(`${res.message}`).show().fadeOut(3000)
        } else {
            $('#plainModal').modal('hide')
            $('.alert.alert-danger').text(`${res.message}`).show().fadeOut(3000)
        }
    })
})

