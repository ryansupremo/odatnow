        // star rating
        $(document).ready(function () {


            $('.fa-star').click(function() {
                let stars = $('.fa-star')
                let id = $('#id').val()
                let star = $(this).attr('data-star')
                $.each(stars, function (key, value) {
                    if(value.attributes[0].value > star) {
                       value.classList.remove('starYellow')
                    }
                });

                $.get(`/products/optimization/starrating/${id}/${star}`)
                .then(res => {
                    if (res.success === 200) {
                        $.each(stars, function (key, value) {
                             if(value.attributes[0].value <= star) {
                                value.classList.add('starYellow')
                             }
                        });
                    }
                })
                .catch(res => console.log('res:', res))
            })
            // optimization view functions
            id = $('#id').val()




            $('.product-status').click(function () {
                let actualStatus = $(this).attr('data-status')
                let status = ''
                if (actualStatus == 'complete') {
                    status = 'review'
                } else {
                    status = 'complete'
                }
                $.ajax({
                    url: `/products/optimization/ajax/complete/${id}/${status}`,
                    type: "GET",
                    dataType: 'JSON',
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                        // "Content-Type": "application/x-www-form-urlencoded"
                    },
                })
                .then(response => {
                    if (response.success === 200 ) {
                        if (response.status == 'complete') {
                            $(this).removeClass('text-danger').addClass('text-success')
                            $(this).attr('data-status', response.status)
                        } else {
                            $(this).removeClass('text-success').addClass('text-danger')
                            $(this).attr('data-status', response.status)
                        }
                    }
                })
            })
            // remote feature functions

            $('.remoteFeature').click(function(){
                // id = $('#id').val()
                let type = $(this).attr('data-type')
                let status = $(this).attr('data-status')
                let color = ''
                if (type == 'bluetooth') {
                    color = 'text-bluetooth'
                } else if (type == 'wifi'){
                    color = 'text-warning'
                } else if (type == 'rf'){
                    color = 'text-success'
                } else if (type == 'infrared') {
                    color = 'text-danger'
                }

                $.get(`/products/optimization/wireless/${type}/${id}`)
                    .then(res => {
                        if (res.success === 200) {
                            if (status == 'on') {
                                $(this).removeClass(color)
                                $(this).attr('data-status', 'off')
                            } else {
                                $(this).addClass(color)
                                $(this).attr('data-status', 'on')
                            }
                        }
                    })
            })

            let image = $('.image')
            if (image.width() > image.height()) {
                image.css({
                    width: '90px',
                    height: '51px',
                })
                $('#zoomImage').css({
                    width: '600px',
                    height: '400px',
                })
            } else {
                image.css({
                    width: '51px',
                    height: '90px',
                })
                $('#zoomImage').css({
                    width: '400px',
                    height: '600px',
                })
            }
            $('#ex1').zoom();

            $('#amazonSwitch').change(function(){
                $('#amazonSwitch').is(':checked') ? $('#labelAmazon').html('On') : $('#labelAmazon').html('Off')
            })
            $('#ebaySwitch').change(function(){
                $('#ebaySwitch').is(':checked') ? $('#labelEbay').html('On') : $('#labelEbay').html('Off')
            })
            $('#googleSwitch').change(function(){
                $('#googleSwitch').is(':checked') ? $('#labelGoogle').html('On') : $('#labelAmazon').html('Off')
            })
        })







