<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedProductsMls extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'related_products_mls';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'store_id',
                  'related_products'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the ProductsMultiStoreTable for this model.
     *
     * @return App\Models\ProductsMultiStoreTable
     */
    public function ProductsMultiStoreTable()
    {
        return $this->belongsTo('App\Models\ProductsMultiStoreTable','product_id','id');
    }

    /**
     * Get the store for this model.
     *
     * @return App\Models\Store
     */
    public function store()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }



}
