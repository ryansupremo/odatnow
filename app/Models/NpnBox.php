<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NpnBox extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'npn_box';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'box_number',
                  'user_id',
                  'date_created'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    /**
     * Get the npnItems for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function npnItems()
    {
        return $this->hasMany('App\Models\NpnItem','npn_box_id','id');
    }



}
