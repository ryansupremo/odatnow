<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flags extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'flags';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_flag_id',
        'product_id'
    ];

    public static function getFlags($product)
    {
        $data = Flags::from('flags as f')
            ->select(
                'f.id',
                'f.admin_flag_id',
                'f.product_id',
                'admin_manage_flags.name',
                'admin_manage_flags.platform'
            )
            ->leftJoin('admin_manage_flags', 'admin_manage_flags.id', '=', 'f.admin_flag_id')
            ->where('f.product_id', $product->id)
            ->get();

        return $data;
    }

    public static function productFlags($id)
    {
        $result = Flags::from('flags as f')
            ->select(
                'f.id',
                'f.admin_flag_id',
                'f.product_id',
                'admin_manage_flags.name',
                'admin_manage_flags.platform'
            )
            ->leftJoin('admin_manage_flags', 'admin_manage_flags.id', '=', 'f.admin_flag_id')
            ->where('f.product_id', $id)
            ->get();
        return $result;
    }

    public static function deleteFlag($flag, $id)
    {
        $result = Flags::where('admin_flag_id', $flag)
            ->where('product_id', $id)
            ->first();
        $result->delete();
        return $result;
    }
}
