<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuPages extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_pages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'page_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'directory_level',
                  'icon',
                  'page_name',
                  'page_link',
                  'parent',
                  'has_child',
                  'page_order'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the page for this model.
     *
     * @return App\Models\Page
     */
    public function page()
    {
        return $this->belongsTo('App\Models\Page','page_id');
    }

    /**
     * Get the usersGroupPages for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function usersGroupPages()
    {
        return $this->hasMany('App\Models\UsersGroupPage','page_id','page_id');
    }



}
