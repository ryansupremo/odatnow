<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoxSubCategories extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'box_sub_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'box_category_id',
                  'sub_category'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the BoxCategory for this model.
     *
     * @return App\Models\BoxCategory
     */
    public function BoxCategory()
    {
        return $this->belongsTo('App\Models\BoxCategory','box_category_id','id');
    }



}
