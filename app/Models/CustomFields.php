<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomFields extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_fields';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'name',
                  'value',
                  'custom_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * Get the custom for this model.
     *
     * @return App\Models\Custom
     */
    public function custom()
    {
        return $this->belongsTo('App\Models\Custom','custom_id');
    }

    /**
     * @param $product
     * @param $fields
     * @return bool
     */
    public static function insertCustomFields($product, $fields)
    {
        for($i = 0; $i < count($fields); $i++){
            $custom = new CustomFields();
            $custom->product_id = $product->id;
            $custom->name = $fields[$i]['name'];
            $custom->value = $fields[$i]['value'];
            $custom->custom_id = $fields[$i]['id'];
            $custom->save();
            return true;
        }
    }

    public static function CustomField($product)
    {
        $data = CustomFields::from("custom_fields as c")
            ->select(
                'c.id',
                'c.product_id',
                'c.name',
                'c.value',
                'c.custom_id'
            )
            ->where('c.product_id' , '=', $product->id)
            ->get()
        ;

        return $data;
    }
    public static function getCustomFieldsByField($product, $field)
    {
        $query = self::CustomField($product);

        $data = array(
            'customId' => 1,
            'value' => ''
        );

        foreach ($query as $q) {
            if ($q->name == $field) {
                $data = array(
                    'customId' => $q->custom_id,
                    'name' => $q->name,
                    'value' => $q->value
                );
            }
        }
        return $data;
    }
}
