<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MpnMarketStatus extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mpn_market_status';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'mpn',
                  'status',
                  'productID'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    public static function mpnStatus($data, $id)
    {
        $result = MpnMarketStatus::where('mpn', $data[0]->mpn)->where('productID', $id)->first();
        return $result;
    }


}
