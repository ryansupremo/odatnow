<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCompatibles extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_compatibles';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'compatibles',
        'search_url',
        'apn1',
        'apn2'
    ];

    public static function locateCompatibles($product)
    {
        $result = ProductCompatibles::from('products_compatibles as c')
            ->select('c.id')
            ->where('c.product_id', $product->id)
            ->take(1)
            ->get()
        ;
        return $result;
    }
}
