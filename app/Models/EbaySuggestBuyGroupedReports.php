<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbaySuggestBuyGroupedReports extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ebay_suggest_buy_grouped_reports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'root_sku',
                  'brand',
                  'mpn',
                  'overall_count',
                  'active_count',
                  'completed_count',
                  'active_to_complete_percentage',
                  'unsold_count',
                  'sold_count',
                  'unsold_to_sold_percentage',
                  'batch',
                  'active_to_sold_percentage',
                  'sold_to_active_percentage'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    



}
