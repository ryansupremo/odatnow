<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipstationAwaitingOrders extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipstation_awaiting_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'order_id',
                  'order_key',
                  'age',
                  'sku',
                  'bin_picking_number',
                  'quantity',
                  'item_name',
                  'recipient',
                  'store',
                  'store_address',
                  'order_number',
                  'order_total',
                  'buyer',
                  'customer_email',
                  'order_date',
                  'ship_date',
                  'ship_to_street',
                  'ship_to_city',
                  'ship_to_postal',
                  'ship_to_phone',
                  'customer_notes',
                  'internal_notes'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the order for this model.
     *
     * @return App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order','order_id');
    }



}
