<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImagesDraft extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_images_drafts';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'image',
        'type',
        'userID',
        'status'
    ];

    public static function getAllImagesById($id)
    {
        $data = ProductImagesDraft::from('product_images_drafts as p')
            ->select(
                'p.id',
                'p.product_id',
                'p.image',
                'p.type',
                'p.userID',
                'u.name',
                'p.status',
                'p.date_added'
            )
            ->where('p.product_id', $id)
            ->where('p.status', 'draft')
            ->leftJoin('users as u', function($leftJoin)
            {
                $leftJoin->on('u.id', '=', 'p.userID');
            })
            ->get();
        return $data;
    }

    public static function locateImage($id)
    {
        $result = ProductImagesDraft::from('product_images_drafts as p')
            ->select('p.id')
            ->where('p.product_id', $id)
            ->take(1)
            ->get()
        ;
        return $result;
    }

    public static function setAllSecondary($id)
    {
        $query = ProductImagesDraft::from('product_images_drafts as p')
            ->select('p.id')
            ->where('p.product_id', $id)
            ->get()
        ;

        foreach ($query as $q) {
            $data = self::find($q->id);
            $data->type = "secondary";
            $data->save();
        }
    }
}
