<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbayOrders extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ebay_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'order_id',
                  'order_status',
                  'amount_paid',
                  'created_time',
                  'tracking_number',
                  'carrier_used',
                  'seller_email',
                  'seller_id',
                  'item_id',
                  'title',
                  'item_condition',
                  'quantity_purchased',
                  'transaction_id',
                  'est_delivery_time_min',
                  'est_delivery_time_max',
                  'sku',
                  'inventory_level',
                  'is_received'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the order for this model.
     *
     * @return App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order','order_id');
    }

    /**
     * Get the seller for this model.
     *
     * @return App\Models\Seller
     */
    public function seller()
    {
        return $this->belongsTo('App\Models\Seller','seller_id');
    }

    /**
     * Get the item for this model.
     *
     * @return App\Models\Item
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Item','item_id');
    }

    /**
     * Get the transaction for this model.
     *
     * @return App\Models\Transaction
     */
    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction','transaction_id');
    }



}
