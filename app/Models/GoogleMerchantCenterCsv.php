<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleMerchantCenterCsv extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'google_merchant_center_csv';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'product_name',
                  'product_type',
                  'product_description',
                  'product_availability',
                  'category',
                  'product_condition',
                  'product_url',
                  'gps_enabled',
                  'gps_category',
                  'sku',
                  'brand',
                  'mpn',
                  'asin_upc',
                  'ebay_eligible',
                  'amazon_eligible',
                  'amazon_pricing',
                  'amazon_no_asin',
                  'amazon_restricted',
                  'amazon_needs_support_call',
                  'amazon_other'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public static function getGoogleCsv($product)
    {
        $data = GoogleMerchantCenterCsv::from("google_merchant_center_csv as g")
            ->select(
                'g.product_id',
                'g.product_name',
                'g.product_type',
                'g.product_description',
                'g.product_availability',
                'g.category',
                'g.product_condition',
                'g.product_url',
                'g.gps_enabled',
                'g.gps_category',
                'g.sku',
                'g.brand',
                'g.mpn',
                'g.asin_upc',
                'g.ebay_eligible',
                'g.amazon_eligible',
                'g.amazon_pricing',
                'g.amazon_no_asin',
                'g.amazon_restricted',
                'g.amazon_needs_support_call',
                'g.amazon_other'
            )
            ->where('g.product_id' , '=', $product->id)
            ->get()
        ;
        return $data;
    }

}
