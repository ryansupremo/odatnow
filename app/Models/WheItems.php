<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WheItems extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'whe_items';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'load_id',
                  'whe_box_id',
                  'mpn',
                  'brand',
                  'whe_sku',
                  'user_id',
                  'date_created'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the Load for this model.
     *
     * @return App\Models\Load
     */
    public function Load()
    {
        return $this->belongsTo('App\Models\Load','load_id','id');
    }

    /**
     * Get the WheBox for this model.
     *
     * @return App\Models\WheBox
     */
    public function WheBox()
    {
        return $this->belongsTo('App\Models\WheBox','whe_box_id','id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }



}
