<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignTo extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assign_to';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'images',
        'videos',
        'blog',
        'manuals',
        'seo',
        'data_check',
        'price_check',
        'facebook',
        'twitter',
        'inventory',
        'comments',
        'userID'
    ];

    public static function locateItems($product)
    {
        $result = AssignTo::from('assign_to as a')
            ->select('a.id')
            ->where('a.product_id', $product->id)
            ->take(1)
            ->get()
        ;
        return $result;
    }
}
