<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductsMultiStoreTable extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_multi_store_table';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'store_id',
                  'product_id',
                  'name',
                  'type',
                  'sku',
                  'mpn',
                  'description',
                  'weight',
                  'width',
                  'depth',
                  'height',
                  'price',
                  'cost_price',
                  'retail_price',
                  'sale_price',
                  'tax_class_id',
                  'product_tax_code',
                  'brand_id',
                  'inventory_level',
                  'inventory_warning_level',
                  'inventory_tracking',
                  'fixed_cost_shipping_price',
                  'is_free_shipping',
                  'is_visible',
                  'is_featured',
                  'warranty',
                  'bin_picking_number',
                  'layout_file',
                  'upc',
                  'search_keywords',
                  'availability',
                  'availability_description',
                  'gift_wrapping_options_type',
                  'sort_order',
                  'product_condition',
                  'is_condition_shown',
                  'order_quantity_minimum',
                  'order_quantity_maximum',
                  'page_title',
                  'meta_description',
                  'view_count',
                  'preorder_release_date',
                  'preorder_message',
                  'is_preorder_only',
                  'is_price_hidden',
                  'price_hidden_label',
                  'calculated_price',
                  'date_created',
                  'date_modified',
                  'variant_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the store for this model.
     *
     * @return App\Models\Store
     */
    public function store()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    /**
     * Get the taxClass for this model.
     *
     * @return App\Models\TaxClass
     */
    public function taxClass()
    {
        return $this->belongsTo('App\Models\TaxClass','tax_class_id');
    }

    /**
     * Get the brand for this model.
     *
     * @return App\Models\Brand
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand','brand_id');
    }

    /**
     * Get the variant for this model.
     *
     * @return App\Models\Variant
     */
    public function variant()
    {
        return $this->belongsTo('App\Models\Variant','variant_id');
    }

    /**
     * Get the bulkPricingRulesMl for this model.
     *
     * @return App\Models\BulkPricingRulesMl
     */
    public function bulkPricingRulesMl()
    {
        return $this->hasOne('App\Models\BulkPricingRulesMl','product_id','id');
    }

    /**
     * Get the categoriesMl for this model.
     *
     * @return App\Models\CategoriesMl
     */
    public function categoriesMl()
    {
        return $this->hasOne('App\Models\CategoriesMl','product_id','id');
    }

    /**
     * Get the customFieldsMl for this model.
     *
     * @return App\Models\CustomFieldsMl
     */
    public function customFieldsMl()
    {
        return $this->hasOne('App\Models\CustomFieldsMl','product_id','id');
    }

    /**
     * Get the customUrlMl for this model.
     *
     * @return App\Models\CustomUrlMl
     */
    public function customUrlMl()
    {
        return $this->hasOne('App\Models\CustomUrlMl','product_id','id');
    }

    /**
     * Get the giftWrappingOptionsListMl for this model.
     *
     * @return App\Models\GiftWrappingOptionsListMl
     */
    public function giftWrappingOptionsListMl()
    {
        return $this->hasOne('App\Models\GiftWrappingOptionsListMl','product_id','id');
    }

    /**
     * Get the imagesMl for this model.
     *
     * @return App\Models\ImagesMl
     */
    public function imagesMl()
    {
        return $this->hasOne('App\Models\ImagesMl','product_id','id');
    }

    /**
     * Get the metaKeywordsMl for this model.
     *
     * @return App\Models\MetaKeywordsMl
     */
    public function metaKeywordsMl()
    {
        return $this->hasOne('App\Models\MetaKeywordsMl','product_id','id');
    }

    /**
     * Get the relatedProductsMl for this model.
     *
     * @return App\Models\RelatedProductsMl
     */
    public function relatedProductsMl()
    {
        return $this->hasOne('App\Models\RelatedProductsMl','product_id','id');
    }



}
