<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'is_thumbnail',
                  'sort_order',
                  'description',
                  'image_id',
                  'image_file',
                  'url_zoom',
                  'url_standard',
                  'url_thumbnail',
                  'url_tiny',
                  'date_modified',
                  'image_file_name',
                  'user_id',
                  'is_pending',
                  'date_created'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * Get the image for this model.
     *
     * @return App\Models\Image
     */
    public function image()
    {
        return $this->belongsTo('App\Models\Image','image_id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public static function getImageDetails($id)
    {
        $data = Images::from("images as i")
            ->select(
                'i.id',
                'i.product_id',
                'i.is_thumbnail',
                'i.sort_order',
                'i.description',
                'i.image_id',
                'i.image_file',
                'i.url_zoom',
                'i.url_standard',
                'i.url_thumbnail',
                'i.url_tiny',
                'i.date_modified',
                'i.image_file_name'
            )
            ->where('i.product_id', '=', $id)
            ->get()
        ;
        return $data;
    }

}
