<?php

namespace App\Services;
use App\Models\Store;
use App\Models\Brand;
use App\Models\ProductCategories;

class ProductsService
{
    public static function getStore()
    {
        $store = Store::getActiveStore();
        return $store;
    }

    public static function getBrand($brand)
    {
        $brand = Brand::getBrand($brand);
        return $brand;
    }

    public static function getDefaultCategory()
    {
        $defaultCategory = ProductCategories::getDefaultCategory();
        return $defaultCategory;
    }

    public static function getBrandCategory($brandName)
    {
        $brandCategory = ProductCategories::getBrandCategory($brandName);
        return $brandCategory;
    }

}
