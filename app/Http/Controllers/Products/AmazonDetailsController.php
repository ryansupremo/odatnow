<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;

class AmazonDetailsController extends Controller
{
    public function detailsAction($id)
    {
        $page_title = "Amazon Details";
        $data = Products::optimization($id);

        return view('pages.products.amazon.details', [
            'page_title' => $page_title,
            'data' => $data,
        ]);
    }
}
