<?php
namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;

class WirelessController extends Controller
{
    public function toggleBluetoothAction($id)
    {
        $product = Products::find($id);
        if ($product->bluetooth == "on") {
            $product->bluetooth = "off";
            $product->save();
            $state = "off";
        } else {
            $product->bluetooth = "on";
            $product->save();
            $state = "on";
        }
        return response()->json([
            'success' => 200,
            'state' => $state,
        ]);
    }

    public function toggleWifiAction($id)
    {
        $product = Products::find($id);
        if ($product->wifi == "on") {
            $product->wifi = "off";
            $product->save();
            $state = "off";
        } else {
            $product->wifi = "on";
            $product->save();
            $state = "on";
        }
        return response()->json([
            'success' => 200,
            'state' => $state,
        ]);
    }

    public function toggleRfAction($id)
    {
        $product = Products::find($id);
        if ($product->rf == "on") {
            $product->rf = "off";
            $product->save();
            $state = "off";
        } else {
            $product->rf = "on";
            $product->save();
            $state = "on";
        }
        return response()->json([
            'success' => 200,
            'state' => $state,
        ]);
    }

    public function toggleInfraRedAction($id)
    {
        $product = Products::find($id);
        if ($product->infrared == "on") {
            $product->infrared = "off";
            $product->save();
            $state = "off";
        } else {
            $product->infrared = "on";
            $product->save();
            $state = "on";
        }
        return response()->json([
            'success' => 200,
            'state' => $state,
        ]);
    }
}
