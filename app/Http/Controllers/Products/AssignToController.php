<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AssignTo;
use App\Models\Products;

class AssignToController extends Controller
{
    public function assignToAction($id, Request $request)
    {
        $product = Products::find($id);

        /**
         * Create a model view with the following fields:
         *
         * id : hidden : value: $id
         * images : string : value: on or off
         * videos : string : value: on or off
         * blog : string : value: on or off
         * manuals : string : value: on or off
         * seo : string : value: on or off
         * data_check : string : value: on or off
         * price_check : string : value: on or off
         * facebook : string : value: on or off
         * twitter : string : value: on or off
         * inventory : string : value: on or off
         * comments : text : any value or null
         */

        // create a view and pass the data back to the controller.
    }

    public function saveAction(Request $request)
    {
        $id = $request->request->get('id');

        $product = Products::find($id);
        $user = $request->user();
        $query = AssignTo::locateItems($product);

        $images = $request->request->get('images');
        $videos = $request->request->get('videos');
        $blog = $request->request->get('blog');
        $manuals = $request->request->get('manuals');
        $seo = $request->request->get('seo');
        $data_check = $request->request->get('data_check');
        $price_check = $request->request->get('price_check');
        $facebook = $request->request->get('facebook');
        $twitter = $request->request->get('twitter');
        $inventory = $request->request->get('inventory');
        $comments = $request->request->get('comments');

        // Check for errors
        $error = false;

        if (($images != "on") && ($images != "off")) {
            $error = true;
        }

        if (($videos != "on") && ($videos != "off")) {
            $error = true;
        }

        if (($blog != "on") && ($blog != "off")) {
            $error = true;
        }

        if (($manuals != "on") && ($manuals != "off")) {
            $error = true;
        }

        if (($seo != "on") && ($seo != "off")) {
            $error = true;
        }

        if (($data_check != "on") && ($data_check != "off")) {
            $error = true;
        }

        if (($price_check != "on") && ($price_check != "off")) {
            $error = true;
        }

        if (($facebook != "on") && ($facebook != "off")) {
            $error = true;
        }

        if (($twitter != "on") && ($twitter != "off")) {
            $error = true;
        }

        if (($inventory != "on") && ($inventory != "off")) {
            $error = true;
        }

        if ($error === true) {
            return response()->json([
                'success' => 502,
                'message' => 'One or more data violation errors was detected.',
            ]);
        }

        $found = 0;
        foreach ($query as $q) {
            $found = 1;
            if (is_object($q)) {
                // update
                $assign = AssignTo::find($q->id);
                $assign->product_id = $product->id;
                $assign->images = $images;
                $assign->videos = $videos;
                $assign->blog = $blog;
                $assign->manuals = $manuals;
                $assign->seo = $seo;
                $assign->data_check = $data_check;
                $assign->price_check = $price_check;
                $assign->facebook = $facebook;
                $assign->twitter = $twitter;
                $assign->inventory = $inventory;
                $assign->comments = $comments;
                $assign->date_updated = new \DateTime();
                $assign->save();
            }
        }

        if ($found == 0) {
            // new
            $assign = new AssignTo();
            $assign->product_id = $product->id;
            $assign->images = $images;
            $assign->videos = $videos;
            $assign->blog = $blog;
            $assign->manuals = $manuals;
            $assign->seo = $seo;
            $assign->data_check = $data_check;
            $assign->price_check = $price_check;
            $assign->facebook = $facebook;
            $assign->twitter = $twitter;
            $assign->inventory = $inventory;
            $assign->comments = $comments;
            $assign->userID = $user->id;
            $assign->date_added = new \DateTime();
            $assign->date_updated = new \DateTime();
            $assign->save();
        }

        return response()->json([
            'success' => 200,
            'message' => 'The fields was saved.',
        ]);
    }
}
