<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BigCommerceApi;
use App\Services\AwsService;
use App\Models\Products;
use App\Models\ProductImagesDraft;
use App\Models\ProductImages;

class ImageProcessController extends Controller
{
    public function processImageAction($id, $imageID, AwsService $aws, BigCommerceApi $bc)
    {
        $draft = ProductImagesDraft::find($imageID);

        // copy image to AWS
        $filenameWithPath = public_path('products') . "/" . $draft->image;
        $result = $aws->putObject(env('AWS_BUCKET'), $filenameWithPath, 'images/products/raw/' . $draft->image);

        print "<pre>";
        print_r($result);
        print "</pre>";

        print "Ok!";
        die;
    }
}
