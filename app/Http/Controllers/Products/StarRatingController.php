<?php
namespace App\Http\Controllers\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;

class StarRatingController extends controller
{
    public function toggleStarAction($id, $star)
    {
        $product = Products::find($id);

        $state = "0";
        switch($star){
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
                $state = "1";
                break;

            default:
                $state = "0";
                break;
        }

        if ($state == "0") {
            return response()->json([
                'success' => 502,
                'message' => 'The star should be a value 1 to 5',
            ]);
        }

        $product->star_rating = $star;
        $product->save();

        return response()->json([
            'success' => 200,
        ]);
    }
}
