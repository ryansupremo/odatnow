<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GoogleAnalyticsController extends Controller
{
    public function listAction()
    {
        $analytics = $this->initializeAnalytics();
        $pageViews = $this->getData($analytics, 'yesterday', 'yesterday', 'ga:pageviews', 'pageviews');
        $test = $this->getData($analytics, 'yesterday', 'yesterday', 'ga:adClicks', 'adClicks');
        $sessions = $this->getData($analytics, 'yesterday', 'yesterday', 'ga:sessions', 'sessions');
        $users = $this->getData($analytics, 'yesterday', 'yesterday', 'ga:users', 'users');
        //above function can be used to query yesterday data, then execute in table at the certain time of the day

        //$test = $this->getData2($analytics);

        $pageViews7 = $this->getData($analytics, '7daysAgo', 'yesterday', 'ga:pageviews', 'pageviews');
        $sessions7 = $this->getData($analytics, '7daysAgo', 'yesterday', 'ga:sessions', 'sessions');
        $users7 = $this->getData($analytics, '7daysAgo', 'yesterday', 'ga:users', 'users');
        //

        $pageViews30 = $this->getData($analytics, '30daysAgo', 'yesterday', 'ga:pageviews', 'pageviews');
        $sessions30 = $this->getData($analytics, '30daysAgo', 'yesterday', 'ga:sessions', 'sessions');
        $users30 = $this->getData($analytics, '30daysAgo', 'yesterday', 'ga:users', 'users');

        return view('pages.products.google_analytics.list', [
            'pageviews' => $pageViews,
            'sessions' => $sessions,
            'users' => $users,

            'pageviews7' => $pageViews7,
            'sessions7' => $sessions7,
            'users7' => $users7,

            'pageviews30' => $pageViews30,
            'sessions30' => $sessions30,
            'users30' => $users30,
        ]);
    }

    private function initializeAnalytics()
    {
        $json = Storage::disk('public')->get('odatJson.json');
        $json = json_decode($json, true);
        $KEY_FILE_LOCATION = $json;
        $client = new \Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_AnalyticsReporting($client);
        return $analytics;
    }

    private function getData2($analytics)
    {
        $VIEW_ID = "155130774";

        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("2020-09-15");
        $dateRange->setEndDate("2020-09-15");

        // Create the Metrics object.
        $sessions = new \Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:organicSearches");
        $sessions->setAlias("organicSearches");

        //Create the Dimensions object.
        $browser = new \Google_Service_AnalyticsReporting_Dimension();
        $browser->setName("ga:source");

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setDimensions(array($browser));
        $request->setMetrics(array($sessions));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request) );
        return $analytics->reports->batchGet( $body );
    }

    private function getData($analytics, string $startDate, string $endDate, string $gaData, string $gaAlias)
    {
        $VIEW_ID = "155130774";
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDate);
        $dateRange->setEndDate($endDate);

        $sessions = new \Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression($gaData);
        $sessions->setAlias($gaAlias);
        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($sessions));
        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));
        $data = $analytics->reports->batchGet($body);
        return $this->printResults($data);
    }

    private function printResults($reports)
    {
        $data = '';
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                if ($dimensions !== null) {
                    for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                        print($dimensionHeaders[$i] . ": " . $dimensions[$i]);
                    }
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        $data = $values[$k];
                    }
                }
            }
        }
        if ($data === "" || $data === null) {
            $data = 0;
        }
        return $data;
    }

}
