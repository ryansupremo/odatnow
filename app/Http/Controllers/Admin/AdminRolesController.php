<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Role;

class AdminRolesController extends Controller
{
    public function listAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = Role::orderBy('name')->paginate($_ENV['PAGINATE']);

        return view('pages.admin.roles.list', [
            'data' => $data,
        ]);
    }

    public function newAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        return view('pages.admin.roles.new');
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        Role::create($request->all());

        return redirect()->route('admin.roles.list')->with('message', 'The role was added');
    }

    public function showAction($id, Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = Role::find($id);
        return view('admin.roles.show', [
            'data' => $data,
        ]);
    }

    public function editAction($id, Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = Role::find($id);
        return view('pages.admin.roles.edit', [
            'data' => $data,
        ]);
    }

    public function updateAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $id = $request->request->get('id');
        $data = Role::find($id);
        // add the other fields
        $data->name = $request->request->get('name');
        $data->slug = $request->request->get('slug');
        $data->save();

        return redirect()->route('admin.roles.list')->with('message', 'The role was updated');
    }
}
