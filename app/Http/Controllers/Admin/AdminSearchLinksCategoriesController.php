<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SearchLinksCategories;

class AdminSearchLinksCategoriesController extends Controller
{
    public function listAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = SearchLinksCategories::orderBy('title')->paginate($_ENV['PAGINATE']);

        return view('pages.admin.searchlinkscategories.list', [
            'data' => $data,
        ]);
    }

    public function newAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        return view('pages.admin.searchlinkscategories.new');
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'title' => 'required',
            'placement' => 'required',
            'image' => 'required'
        ]);

        SearchLinksCategories::create($request->all());

        return redirect()->route('admin.searchlinkscategories.list')->with('message', 'The category was added');

    }

    public function editAction($id, Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = SearchLinksCategories::find($id);
        return view('pages.admin.searchlinkscategories.edit', [
            'data' => $data,
        ]);

    }

    public function updateAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $id = $request->request->get('id');
        $data = SearchLinksCategories::find($id);
        // add the other fields
        $data->name = $request->request->get('name');
        $data->type = $request->request->get('type');
        $data->title = $request->request->get('title');
        $data->placement = $request->request->get('placement');
        $data->image = $request->request->get('image');
        $data->date_updated = new \DateTime();
        $data->save();

        return redirect()->route('admin.searchlinkscategories.list')->with('message', 'The category was updated');

    }
}
