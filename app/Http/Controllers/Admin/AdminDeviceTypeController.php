<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DeviceType;

class AdminDeviceTypeController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = "Device Type";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = DeviceType::orderBy('device')->paginate($_ENV['PAGINATE']);

        return view('pages.admin.device.list', [
            'page_title' => $page_title,
            'data' => $data,
        ]);
    }

    public function newAction(Request $request)
    {
        $page_title = "Device Type";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        return view('pages.admin.device.new', [
            'page_title' => $page_title,
        ]);
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $request->validate([
            'device' => 'required'
        ]);

        DeviceType::create($request->all());

        return redirect()->route('admin.devicetype.list')->with('message', 'The device was added');
    }

    public function showAction($id, Request $request)
    {

    }

    public function editAction($id, Request $request)
    {
        $page_title = "Device Type";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = DeviceType::find($id);
        return view('pages.admin.device.edit', [
            'page_title' => $page_title,
            'data' => $data,
        ]);
    }

    public function updateAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $id = $request->request->get('id');
        $data = DeviceType::find($id);
        // add the other fields
        $data->device = $request->request->get('device');
        $data->save();

        return redirect()->route('admin.devicetype.list')->with('message', 'The device was updated');
    }
}
