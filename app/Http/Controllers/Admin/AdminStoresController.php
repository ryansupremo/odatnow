<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Http\Request;

class AdminStoresController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = 'Stores';
        $search = $request->search;
        $stores = Store::where( function($query)use ($search) {
            if ($search != "") {
                $query->where('name', 'like', '%'.$search.'%');
            }
        })->orderBy('id', 'DESC')->paginate($_ENV['PAGINATE']);

        return view('pages.admin.stores.list', [
            'stores' => $stores,
            'page_title' => $page_title
        ]);
    }

    public function newAction()
    {
        $page_title = 'Create new store';

        return view('pages.admin.stores.new', [
            'page_title' => $page_title,
        ]);
    }

    public function saveAction(Request $request)
    {
        $validation = $request->validate([
            'store_id' => 'required|string|max:64|unique:store,store_id',
            'name' => 'required|string',
            'url' => 'required|string|url|unique:store,url',
            'client_id' => 'required|string|max:64',
            'token' => 'required|string',
            'is_main' => 'required|numeric'
        ]);

        $store = new Store();
        $store->store_id = $request->store_id;
        $store->name = $request->name;
        $store->url = $request->url;
        $store->client_id = $request->client_id;
        $store->token = $request->token;
        $store->is_main = $request->is_main;
        $store->save();

        return redirect()->route('admin.stores.list')->with('message', "Store was successfully created");
    }

    public function editAction($id)
    {
        $page_title = 'Edit Store';
        $store = Store::find($id);

        return view('pages.admin.stores.edit', [
            'page_title' => $page_title,
            'store' => $store
        ]);
    }
    public function updateAction(Request $request)
    {
        $validation = $request->validate([
            'name' => 'required|string',
            'url' => 'required|string|url',
            'client_id' => 'required|string|max:64',
            'token' => 'required|string',
            'is_main' => 'required|numeric'
        ]);
        $store = Store::find($request->id);
        if ($store->name != $request->name){
            $store->name = $request->name;
        }
        if ($store->url != $request->url){
            $existing_url = Store::where('url', $request->url)->first();
            if ($existing_url){
                return redirect()->back()->with('message', 'This url is already in our database');
            }
            $store->url = $request->url;
        }
        if ($store->client_id != $request->client_id){
            $store->client_id = $request->client_id;
        }
        if ($store->token != $request->token){
            $store->token = $request->token;
        }
        if ($store->is_main != $request->is_main){
            $store->is_main = $request->is_main;
        }
        $store->save();

        return redirect()->route('admin.stores.list')->with('message', 'Store was successfully updated!');
    }
}
