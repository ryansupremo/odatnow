<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

class LogoutController extends Controller
{
    public function logoutAction()
    {
        Auth::logout();
        return redirect()->route('dashboard')->with('message', 'You have been logged out.');
    }
}
