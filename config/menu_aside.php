<?php
// Aside menu
return [

    'items' => [
        // Dashboard
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/',
            'new-tab' => false,
        ],

        // Custom
        [
            'title' => 'Products',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Add',
                    'bullet' => 'dot',
                    'page' => 'products/new',
                ],
                [
                    'title' => 'List',
                    'bullet' => 'dot',
                    'page' => 'products/list',
                ],
                [
                    'title' => 'Return Inventory',
                    'bullet' => 'dot',
                    'page' => 'products/return',
                ],
            ]
        ],

        [
            'title' => 'Purchasing',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Ebay Search',
                    'bullet' => 'dot',
                    'page' => 'purchasing/tbd',
                ],
                [
                    'title' => 'Raw Data',
                    'bullet' => 'dot',
                    'page' => 'purchasing/tbd',
                ],
                [
                    'title' => 'Ebay Orders',
                    'bullet' => 'dot',
                    'page' => 'purchasing/tbd',
                ],
            ]
        ],

        [
            'title' => 'Ship Station',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Pending Orders',
                    'bullet' => 'dot',
                    'page' => 'shipstation/tbd',
                ],
            ]
        ],

        [
            'title' => 'Reports',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Product Logs',
                    'bullet' => 'dot',
                    'page' => 'reports/tbd',
                ],
                [
                    'title' => 'Purchasing Logs',
                    'bullet' => 'dot',
                    'page' => 'reports/tbd',
                ],
                [
                    'title' => 'Timestamps',
                    'bullet' => 'dot',
                    'page' => 'reports/tbd',
                ],
                [
                    'title' => 'Days In Inventory',
                    'bullet' => 'dot',
                    'page' => 'reports/tbd',
                ],
            ]
        ],

        [
            'title' => 'Admin',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Manage Roles',
                    'bullet' => 'dot',
                    'page' => 'admin/roles',
                ],
                [
                    'title' => 'Manage Users',
                    'bullet' => 'dot',
                    'page' => 'admin/users',
                ],
                [
                    'title' => 'Device Types',
                    'bullet' => 'dot',
                    'page' => 'admin/devicetype',
                ],
                [
                    'title' => 'Manage Flags',
                    'bullet' => 'dot',
                    'page' => 'admin/flags',
                ],
                [
                    'title' => 'Manage Search Link Categories',
                    'bullet' => 'dot',
                    'page' => 'admin/searchlinkscategories',
                ],
                [
                    'title' => 'Developer Queue',
                    'bullet' => 'dot',
                    'page' => 'admin/developerqueue',
                ],
                [
                    'title' => 'Manage Stores',
                    'bullet' => 'dot',
                    'page' => 'admin/stores',
                ],
                [
                    'title' => 'Manage Brands',
                    'bullet' => 'dot',
                    'page' => 'admin/brands',
                ]
            ]
        ],

        [
            'title' => 'Bulk',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Bulk Updates',
                    'bullet' => 'dot',
                    'page' => 'bulk/list',
                ],
            ]
        ],

        [
            'title' => 'Miscellaneous',
            'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
            'bullet' => 'line',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Ebay Customer Search',
                    'bullet' => 'dot',
                    'page' => 'ebay/tbd',
                ],
                [
                    'title' => 'Ebay Rough Search',
                    'bullet' => 'dot',
                    'page' => 'ebay/tbd',
                ],
            ]
        ],

    ]

];
